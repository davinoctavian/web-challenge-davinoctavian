import React, { useState } from "react";
import './Login.css';
import logo from "../../assets/logo.png";
import headerLogin from '../../assets/header-login.png';


const styles = {
    headerContainer: {
        backgroundImage: `url(${headerLogin})`
    }
};

function Login() {
  localStorage.removeItem("userLogged")
  // React States
  const [errorMessages, setErrorMessages] = useState({});

  // // User Login info
  // const database = [
  //   {
  //     uid: "user1",
  //     password: "pass1"
  //   },
  //   {
  //     uid: "user2",
  //     password: "pass2"
  //   }
  // ];

  const errors = {
    uid: "User ID salah.",
    pass: "Password salah.",
    notcompleted: "User ID dan atau Password anda belum diisi."
  };

  const handleSubmit = (event) => {
    //Prevent page reload
    event.preventDefault();

    var { uid, pass } = document.forms[0];
    if (!uid.value || !pass.value) {
        setErrorMessages({ name: "notcompleted", message: errors.notcompleted });
    }
    else {
      localStorage.setItem("userLogged", true)
      window.location.href="/"
    }

    // // Find user login info
    // const userData = database.find((user) => user.uid === uid.value);

    // // Compare user info
    // if (userData) {
    //   if (userData.password !== pass.value) {
    //     // Invalid password
    //     setErrorMessages({ name: "pass", message: errors.pass });
    //   } else {
    //     localStorage.setItem("userLogged", true)
    //     window.location.href="/"
    //   }
    // } else {
    //   // Username not found
    //   setErrorMessages({ name: "uid", message: errors.uid });
    // }
  };

  // Generate JSX code for error message
  const renderErrorMessage = (name) =>
    name === errorMessages.name && (
      <div className="error">{errorMessages.message}</div>
    );

  // JSX code for login form
  const renderForm = (
    <div className="form">
      <form onSubmit={handleSubmit}>
        {renderErrorMessage("notcompleted")}
        <div className="input-container">
          <label>User ID </label>
          <input type="text" name="uid" placeholder="User ID" />
          {renderErrorMessage("uid")}
        </div>
        <div className="input-container">
          <label>Password </label>
          <input type="password" name="pass" placeholder="password" />
          {renderErrorMessage("pass")}
        </div>
        <div className="button-container">
          <input type="submit" value="LOGIN"/>
        </div>
      </form>
    </div>
  );

  return (
    <div className="login-form" style={styles.headerContainer}>
      <div className="logo-container"><img src={logo} alt="logo"/></div>
      <div className="title-container">
        <div className="title">Login</div>
        <div className="title-desc">Please sign in to continue.</div>
      </div>
      {renderForm}
      <div className="link-signup">don't have an account? <a href="#">Sign Up</a></div>
    </div>
  );
}

export default Login;
