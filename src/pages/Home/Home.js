import './Home.css';
import React, { useState, useEffect } from "react"
import { Link } from "react-router-dom"
import Form from "../../components/Form"
import Table from "../../components/Table"

import { httpHelper } from "../../helpers/httpHelper"
import headerLogo from '../../assets/header-splash.png';
import footerLogo from '../../assets/footer-splash.png';

const styles = {
    headerContainer: {
        backgroundImage: `url(${headerLogo})`
    }
}

const Home = () => {
  const [users, setUsers] = useState(null)
  let userLogged
  userLogged = localStorage.getItem("userLogged")
  if (!userLogged) {
    window.location.href="/login"
  }

	const url = "http://localhost:5000/users"
	const api = httpHelper()

	useEffect(() => {
		getUsers()
	}, [])

	const postUser = user => {
		api
			.post(`${url}`, { body: user })
			.then(res => getUsers())
			.catch(err => console.log(err))
	}

	const updateUser = (id, user) => {
		api
			.put(`${url}/${id}`, { body: user })
			.then(res => getUsers())
			.catch(err => console.log(err))
	}

	const deleteUser = id => {
		api
			.del(`${url}/${id}`, {})
			.then(res => getUsers())
			.catch(err => console.log(err))
	}

	const getUsers = () => {
		api
			.get(`${url}?_expand=companies`)
			.then(res => {
				setUsers(res)
			})
			.catch(err => console.log(err))
	}

	if (!users) return null

	return (
    <div className="home-container" style={styles.headerContainer}>
      <header>
        <Link to="/login">Logout</Link>
      </header>
      <main>
        <h3>New user</h3>
        <Form postUser={postUser} />
        <div className='all-users'>
          <h3>All users</h3>
          <Table
            users={users}
            setUsers={setUsers}
            postUser={postUser}
            updateUser={updateUser}
            deleteUser={deleteUser}
          />
        </div>
      </main>
      <div className="footer-container"><img src={footerLogo} alt="logo"/></div>
    </div>
	)
}

export default Home;